<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Home</title>
	<style type="text/css">
		body{
			padding: 0;
			margin: 0;
			font-family: "open-sans", sans-serif;
		}

		div{
			box-sizing: border-box;
		}

		ul.nav{
			list-style-type: none;
			padding: 0;
			margin: 0;
			background: #888;
		}

		ul.nav li{
			display: inline-block;
		}

		ul.nav li a{
			display: inline-block;
			line-height: 40px;
			padding: 0 20px;
			margin: 0;
			text-decoration: none;
			color: #fff;
		}

		ul.nav li a:link,
		ul.nav li a:visited{
			background: #888;
		}

		ul.nav li a:hove,
		ul.nav li a:active{
			background: #888;
		}

		.header{
			height: 100px;
			padding: 15px;
			color: #fff;
			font-size: 1rem;
			background-color: #000;
		}

		.container{
			width: 100%;
			max-width: 960px;
			margin: 0 auto;
			padding: 30px;
			min-height: 600px;
		}

		.footer{
			height: 100px;
			padding: 30px;
			color: #fff;
			font-size: 1rem;
			background-color: #000;
			text-align: center;
		}
	</style>
</head>
<body>
	
	<div class="nav">
		<ul class="nav">
			<li><a class="current" href="#">Home</a></li>
			<li><a href="#">About</a></li>
			<li><a href="#">Shop</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">FAQ</a></li>
			<li><a href="#">Contact</a></li>
		</ul>
	</div><!-- /nav -->
	<div class="header">
		<h1>PHP Test Site</h1>
	</div><!-- /header -->